graph = {'A': ['C', 'E'],
         'B': ['A','D', 'E'],
         'C': ['A', 'F', 'G'],
         'D': ['B'],
         'E': ['A', 'B','D'],
         'F': ['C'],
         'G': ['C']}
explored = []
def bfs(graph, start):
   global explored
   queue = [start]
   while queue:
       node = queue.pop(0)
       if node not in explored:
           explored.append(node)
           neighbours = graph[node]
           for neighbour in neighbours:
               queue.append(neighbour)
   return explored
bfs(graph,'A')
print(explored)    
