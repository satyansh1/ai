#1 for lion and 0 for man
l1=1;l2=1;l3=1;
m1=0;m2=0;m3=0;
a=[l1,l2,l3,m1,m2,m3];
b=[];
print("left riverbank ",a)
print("right riverbank ",b)
print("-------------------------------------------------------");
#step 1
a.remove(l1);
b.append(l1);
a.remove(l2);
print("left riverbank ",a)
print("right riverbank ",b)
print("-------------------------------------------------------");

#step 2
a.remove(m3);
b.append(m3);
print("left riverbank ",a)
print("right riverbank ",b)
print("-------------------------------------------------------");

#step 3
a.remove(m1);
b.append(m1);
print("left riverbank ",a)
print("right riverbank ",b)
print("-------------------------------------------------------");

#step 4
a.remove(l3);
b.append(l3);
print("left riverbank ",a)
print("right riverbank ",b)
print("-------------------------------------------------------");
#step 5
b.append(l2);
a.remove(m2);
b.append(m2);
print("left riverbank ",a)
print("right riverbank ",b)
print("-------------------------------------------------------");

