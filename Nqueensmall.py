from itertools import permutations
n = int(input("Enter number of queens : "))
sols=0
cols=range(n)
total=0
for combo in permutations(cols):
    if n == len(set(combo[i]+i for i in cols))==len(set(combo[i]-i for i in cols)):
        sols += 1
        print("solution"+str(sols)+":"+str(combo)+"\n")
        print("\n".join(" o "*i+" X "+" o "*(n-i-1) for i in combo)+"\n\n\n\n")
        total = total +1
    print(total)
